const { src, dest, parallel } = require('gulp');

const include = require('gulp-include');

const pug = require('gulp-pug');

const sass = require('gulp-sass');
const minifyCSS = require('gulp-csso');

// const uglify = require('gulp-uglify');
const concat = require('gulp-concat');


function html() {
    return src('pug/*.pug')
        .pipe(pug())
        .pipe(dest('.'))
}

function css() {
    return src('sass/*.sass')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(minifyCSS())
        .pipe(dest('.'))
}

function js() {
    return src('js/*.js')
        .pipe(include())
        // .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(dest('.'))
}

exports.js = js;
exports.css = css;
exports.html = html;
exports.default = parallel(html, css, js);